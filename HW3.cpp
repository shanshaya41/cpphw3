#include <iostream>
#include <string>
using namespace std;

int main()
{
	string str;
	cout << "Enter a string: ";
	getline(cin, str);

	cout << "Length of your string = " << str.size() << endl;
	cout << "The first symbol - " << str[0] << endl;
	cout << "The last symbol - " << str[str.size()-1]<<endl;

return 0;
}